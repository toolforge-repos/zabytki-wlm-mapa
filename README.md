# wikiwakacje-map

## Support / Wsparcie

Wspierane są 5 letnie przeglądarki internetowe. Przynajmnie na razie ostatni Firefox na Windows XP również.

https://en.wikipedia.org/wiki/History_of_the_web_browser#Web_browsers_by_year

## Basic usage

Step. 1. Install & build bundle.
```
npm i
npm run build
```
Step. 2. Deploy / serve.
Link (or copy) `app` folder to a webserver (Apache, Nginx...).

## Watch for changes

For live updates you can use `npm run start` (watching code changes).

## Upgrades
```
npx npm-check-updates -u
npm i
```
